//
//  HomeDetailRootViewTests.swift
//  TestOlchaTests
//
//  Created by Shahzod Bektemirov on 2024/04/11.
//

import SnapshotTesting
import XCTest
import UIKit
@testable import TestOlcha

final class HomeDetailRootViewTest: XCTestCase {
    
    override func run() {
        super.run()
        isRecording = false
    }
    
    func test_detailRootView() {
        
        let model = HomeDetailRootView.Model(
            userId: 11,
            id: 22,
            title: "olcha Title",
            description: "olcha test description."
        )
        let view = HomeDetailRootView(frame: CGRect(x: 0, y: 0, width: 100, height: 100))
        view.configureView(model: model)
        assertSnapshot(matching: view, as: .image(size: view.frame.size))
    }
}
