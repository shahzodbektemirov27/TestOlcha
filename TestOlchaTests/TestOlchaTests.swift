//
//  TestOlchaTests.swift
//  TestOlchaTests
//
//  Created by Shahzod Bektemirov on 2024/04/09.
//

//import XCTest
//@testable import TestOlcha
//
//class HomeDetailPresenterTests: XCTestCase {
//
//    func testGetSuccess() async throws {
//       
//        let presenter = HomeDetailPresenter(diContainer: test, id: 1)
//        presenter.view = test
//
//        let expectedResponse = HomeResponse(userId: 33, id: 22, title: "Test", body: " Body")
//        mockNetwork.mockResponse = try await DataResponse(model: expectedResponse, statusCode: 33)
//
//        await presenter.getHomeDetailPostList(id: 34)
//        XCTAssertEqual(presenter.data, expectedResponse)
//        XCTAssertEqual(test.rootViewModel?.userId, 34)
//        XCTAssertEqual(test.rootViewModel?.id, 0)
//        XCTAssertEqual(test.rootViewModel?.title, "Test")
//    }
//}
