//
//  DIContainer.swift
//  TestOlcha
//
//  Created by Shahzod Bektemirov on 2024/04/10.
//

import UIKit

public protocol DIContainerProtocol {
    var network: NetworkService { get }
}

final class DIContainer: DIContainerProtocol {
    var network: NetworkService
    var coreData: CoreData
    
    static let shared = DIContainer(
        network: NetworkService.shared,
        coreData: CoreData.shared
    )
    
    init(network: NetworkService, coreData: CoreData) {
        self.network = network
        self.coreData = coreData
    }
}
