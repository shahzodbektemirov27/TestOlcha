//
//  Coordinator.swift
//  TestOlcha
//
//  Created by Shahzod Bektemirov on 2024/04/09.
//

import Foundation

protocol Coordinator {
    func start()
    func coordinate(to coordinator: Coordinator)
}

//need refactor should add something
extension Coordinator {
    func coordinate(to coordinator: Coordinator) {
        coordinator.start()
    }
}
