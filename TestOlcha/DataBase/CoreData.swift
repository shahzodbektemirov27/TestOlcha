//
//  CoreData.swift
//  TestOlcha
//
//  Created by Shahzod Bektemirov on 2024/04/11.
//

import UIKit
import CoreData

protocol AnyCoreData {
    var persistent: NSPersistentContainer { get }
    func saveContext()
    func fetchItem() async -> [CRHomeResponse]
    func delete(_ item: CRHomeResponse, completion: @escaping (Error?) -> Void)
}

final public class CoreData: AnyCoreData {
    static let shared = CoreData()
    
    lazy var persistent: NSPersistentContainer = {
        let container = NSPersistentContainer(name: "CoreData")
        
        container.loadPersistentStores(completionHandler: { (storeDescription, error) in
            if let error = error as NSError? {
                fatalError("unresolved error/\(error)")
            }
        })
        
        return container
    }()
    
    func saveContext() {
        let context = persistent.viewContext
        
        if context.hasChanges {
            do {
                try context.save()
                
            } catch {
                let nserror = error as NSError
                fatalError("Unresolved error \(nserror), \(nserror.userInfo)")
            }
        }
    }
    
    func fetchItem() async -> [CRHomeResponse] {
        let context = persistent.viewContext
        let fetchRequest: NSFetchRequest<CRHomeResponse> = CRHomeResponse.fetchRequest()

        do {
            let fetchedItems = try context.fetch(fetchRequest)
            return fetchedItems
        } catch {
            print("Error fetching titles: \(error)")
            return []
        }
    }
    
    func delete(_ item: CRHomeResponse, completion: @escaping (Error?) -> Void) {
        let context = persistent.viewContext
        context.delete(item)

        do {
            try context.save()
            completion(nil)
        } catch {
            completion(error)
        }
    }
}
