//
//  CRHomeResponse+CoreDataProperties.swift
//  TestOlcha
//
//  Created by Shahzod Bektemirov on 2024/04/11.
//
//

import Foundation
import CoreData


extension CRHomeResponse {

    @nonobjc public class func fetchRequest() -> NSFetchRequest<CRHomeResponse> {
        return NSFetchRequest<CRHomeResponse>(entityName: "CRHomeResponse")
    }

    @NSManaged public var title: String?

}

extension CRHomeResponse : Identifiable {

}
