//
//  BaseViewController.swift
//  TestOlcha
//
//  Created by Shahzod Bektemirov on 2024/04/09.
//

import UIKit

open class BaseViewController: UIViewController {
    private var tap = UITapGestureRecognizer()
    
    public init() {
        super.init(nibName: nil, bundle: nil)
        setupUpInit()
    }
    
    public override init(nibName nibNameOrNil: String?, bundle nibBundleOrNil: Bundle?) {
        super.init(nibName: nibNameOrNil, bundle: nibBundleOrNil)
        setupUpInit()
    }

    @available(*, unavailable)
    public required init?(coder _: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    open override func loadView() {
        super.loadView()
        setupUI()
        
        tap = UITapGestureRecognizer(target: self, action: #selector(dismissKeyboard))
        tap.delegate = self
        
        hideKeyboardWhenTappedAround()
        
        view.backgroundColor = .white
    }
    
    open func setupUpInit() {}
    open func setupUI() {}
    
    private func hideKeyboardWhenTappedAround() {
        tap.cancelsTouchesInView = false
        view.addGestureRecognizer(tap)
    }
    
    @objc private func dismissKeyboard() {
        view.endEditing(true)
    }
}

extension BaseViewController: UIGestureRecognizerDelegate {
    public func gestureRecognizerShouldBegin(_: UIGestureRecognizer) -> Bool {
        true
    }
}
