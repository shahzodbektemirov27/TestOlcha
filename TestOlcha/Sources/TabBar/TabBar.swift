//
//  TabBar.swift
//  TestOlcha
//
//  Created by Shahzod Bektemirov on 2024/04/11.
//

import UIKit

final class TabBar: UITabBarController {
    
    var coordinator: TabBarCoordinator?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        view.backgroundColor = .white
    }
}
