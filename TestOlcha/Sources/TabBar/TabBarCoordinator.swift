//
//  TabBarCoordinator.swift
//  TestOlcha
//
//  Created by Shahzod Bektemirov on 2024/04/11.
//

import UIKit

final class TabBarCoordinator: Coordinator {
    private let navigationController: UINavigationController
    
    init(navigationController: UINavigationController) {
        self.navigationController = navigationController
    }
    
    func start() {
        let tabBarController = TabBar()
        tabBarController.coordinator = self
        
        let homeNavigationController = UINavigationController()
        homeNavigationController.tabBarItem = UITabBarItem(tabBarSystemItem: .topRated, tag: 0)
        let homeCoordinator = HomeCoordinator(navigationController: homeNavigationController)
        
        let secondNavigationViewController = UINavigationController()
        secondNavigationViewController.tabBarItem = UITabBarItem(
            tabBarSystemItem: .downloads, tag: 1)
        let secondCoordinator = SecondViewCoordinator(navigationController: secondNavigationViewController)
        
        tabBarController.viewControllers = [homeNavigationController,
                                            secondNavigationViewController]
        
        tabBarController.modalPresentationStyle = .fullScreen
        tabBarController.tabBar.backgroundColor = UIColor.systemGray6
        navigationController.present(tabBarController, animated: true, completion: nil)
        
        coordinate(to: homeCoordinator)
        coordinate(to: secondCoordinator)
    }
}
