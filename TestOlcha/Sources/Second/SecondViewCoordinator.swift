//
//  SecondViewCoordinator.swift
//  TestOlcha
//
//  Created by Shahzod Bektemirov on 2024/04/11.
//

import UIKit

final class SecondViewCoordinator: Coordinator {
    
    private let navigationController: UINavigationController
    
    init(navigationController: UINavigationController) {
        self.navigationController = navigationController
    }
    
    func start() {
        let presenter = SecondPresenter(diContainer: DIContainer.shared)
        presenter.coordinator = self
        let secondViewController = SecondViewController(presenter: presenter)
        presenter.view = secondViewController
        
        navigationController.pushViewController(secondViewController, animated: false)
    }
}
