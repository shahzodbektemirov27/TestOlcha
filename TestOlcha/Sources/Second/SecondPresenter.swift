//
//  SecondPresenter.swift
//  TestOlcha
//
//  Created by Shahzod Bektemirov on 2024/04/11.
//

import UIKit

protocol SecondPresenterProtocol: AnyObject {
    func viewDidLoad()
    func fetchCoreData()
    func deleteCellCoreDate(data: CRHomeResponse, _ indexPath: IndexPath)
    func numberOfRowsInSection() -> Int
    func cellForRowAt(index: Int, tableView: UITableView) -> UITableViewCell
    
    func deleteTableCell(index: IndexPath)
}

final class SecondPresenter: SecondPresenterProtocol {
    
    // Private properties
    private var data = [CRHomeResponse]()
    private let diContainer: DIContainer
    
    weak var view: SecondViewProtocol?
    var coordinator: SecondViewCoordinator?
    
    // MARK: - Init
    init(diContainer: DIContainer) {
        self.diContainer = diContainer
    }
    
    // MARK: - Lifecycle
    func viewDidLoad() {
        fetchCoreData()
    }
}

//MARK: - CORE DATAS
extension SecondPresenter {
    func fetchCoreData() {
        Task {
            let fetchItem = await diContainer.coreData.fetchItem()
            self.data = fetchItem
            
            await MainActor.run {
                self.view?.reloadData()
            }
        }
    }
    
    func deleteCellCoreDate(data: CRHomeResponse, _ indexPath: IndexPath) {
        diContainer.coreData.delete(data, completion: { (error) in
            if let error = error {
                print("Error deleting item: \(error)")
            } else {
                self.data.remove(at: indexPath.row)
                self.view?.reloadData()
            }
        })
    }
}

//MARK: - TABLE METHODS
extension SecondPresenter {
    func numberOfRowsInSection() -> Int {
        return data.count
    }
    
    func cellForRowAt(index: Int, tableView: UITableView) -> UITableViewCell {
        guard let cell = tableView.dequeueReusableCell(
            withIdentifier: "HomeTableViewCell",
            for: IndexPath(row: index, section: 0)
        ) as? HomeTableViewCell else { return UITableViewCell() }
        let item = data[index]
        cell.titleLabel.text = item.title
        
        return cell
    }
    
    func deleteTableCell(index: IndexPath) {
        deleteCellCoreDate(data: data[index.row], index)
    }
}
