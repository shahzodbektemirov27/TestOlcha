//
//  SecondViewController.swift
//  TestOlcha
//
//  Created by Shahzod Bektemirov on 2024/04/11.
//

import UIKit
import CoreData

extension SecondViewController {
    private enum Size {
        static let paddings: CGFloat = 10
        static let heightRowPadding: CGFloat = 80
    }
}

protocol SecondViewProtocol: AnyObject {
    func reloadData()
    func deleteRowsData(index: [IndexPath])
}

final class SecondViewController: BaseViewController {
    //UI
    private let tableView = UITableView()
    
    // Dependencies
    private let presenter: SecondPresenter
    
    // MARK: - Init
    
    init(presenter: SecondPresenter) {
        self.presenter = presenter
        super.init()
    }
    
    // MARK: - Lifecycle
    
    override func viewDidLoad() {
        super.viewDidLoad()
        configureUI()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        presenter.viewDidLoad()
    }
}

extension SecondViewController {
    private func configureUI() {
        view.addSubview(tableView)
        tableView.rowHeight = Size.heightRowPadding
        tableView.register(HomeTableViewCell.self, forCellReuseIdentifier: "HomeTableViewCell")
        tableView.separatorStyle = .none
        tableView.dataSource = self
        tableView.delegate = self
    
        tableView.snp.makeConstraints {
            $0.edges.equalToSuperview()
        }
    }
}

extension SecondViewController: SecondViewProtocol {
    func reloadData() {
        self.tableView.reloadData()
    }
    
    func deleteRowsData(index: [IndexPath]) {
        self.tableView.deleteRows(at: index, with: .fade)
    }
}

extension SecondViewController: UITableViewDataSource, UITableViewDelegate {
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        presenter.numberOfRowsInSection()
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        presenter.cellForRowAt(index: indexPath.row, tableView: tableView)
    }
    
    func tableView(
        _ tableView: UITableView,
        commit editingStyle: UITableViewCell.EditingStyle,
        forRowAt indexPath: IndexPath
    ) {
        guard editingStyle == .delete else { return }
        presenter.deleteTableCell(index: indexPath)
    }
}
