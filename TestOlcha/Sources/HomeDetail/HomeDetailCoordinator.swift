//
//  HomeDetailCoordinator.swift
//  TestOlcha
//
//  Created by Shahzod Bektemirov on 2024/04/10.
//

import UIKit


final class HomeDetailCoordinator: Coordinator {
    
    private let navigationController: UINavigationController
    private var id: Int
    
    init(navigationController: UINavigationController, id: Int) {
        self.navigationController = navigationController
        self.id = id
    }
    
    func start() {
        let presenter = HomeDetailPresenter(diContainer: DIContainer.shared, id: id)
        let homeDetailViewController = HomeDetailViewController(presenter: presenter)
        homeDetailViewController.coordinator = self
        presenter.view = homeDetailViewController
        navigationController.pushViewController(homeDetailViewController, animated: true)
    }
}

