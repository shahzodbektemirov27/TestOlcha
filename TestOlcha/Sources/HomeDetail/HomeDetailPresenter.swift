//
//  HomeDetailPresenter.swift
//  TestOlcha
//
//  Created by Shahzod Bektemirov on 2024/04/10.
//

import Foundation

protocol HomeDetailPresenterProtocol: AnyObject {
    func viewDidload()
    func getHomeDetailPostList(id: Int)
}

final class HomeDetailPresenter: HomeDetailPresenterProtocol {
    
    private(set) var data: HomeResponse? 
    private let diContainer: DIContainer
    private let id: Int
    weak var view: HomeDetailViewControllerProtocol?
    
    init(diContainer: DIContainer, id: Int) {
        self.diContainer = diContainer
        self.id = id
    }
    
    @MainActor func viewDidload() {
        getHomeDetailPostList(id: id)
    }
    
    @MainActor func getHomeDetailPostList(id: Int) {
        Task.detached { [weak self] in
            guard let self else { return }
            
            do {
                let router = HomeRouter.postDetailList(id: id)
                let result: DataResponse<HomeResponse> = try await diContainer.network.request(endPoint: router)
                
                let homeResponseList: HomeResponse = result.model
                
                await MainActor.run {
                    self.data = homeResponseList
                    
                    self.view?.rootViewConfigure(with: .init(
                        userId: homeResponseList.userId ?? 0,
                        id: homeResponseList.id ?? 0,
                        title: homeResponseList.title ?? "",
                        description: homeResponseList.body ?? ""
                    ))
                }
                
            } catch {
                print("Error: \(error)")
            }
        }
    }
}
