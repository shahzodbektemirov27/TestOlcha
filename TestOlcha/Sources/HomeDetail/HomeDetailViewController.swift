//
//  HomeDetailViewController.swift
//  TestOlcha
//
//  Created by Shahzod Bektemirov on 2024/04/10.
//

import UIKit

protocol HomeDetailViewControllerProtocol: AnyObject {
    func rootViewConfigure(with data: HomeDetailRootView.Model)
}

final class HomeDetailViewController: BaseViewController {
    //UI
    private let rootView = HomeDetailRootView()
    
    //Dependency
    private let presenter: HomeDetailPresenter?
    var coordinator: HomeDetailCoordinator?
    
    init(presenter: HomeDetailPresenter) {
        self.presenter = presenter
        super.init()
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        presenter?.viewDidload()
        
        view.addSubview(rootView)
        rootView.snp.makeConstraints { make in
            make.top.equalTo(view.safeAreaLayoutGuide)
            make.left.right.equalToSuperview().inset(10)
            make.bottom.equalTo(view.safeAreaLayoutGuide).multipliedBy(0.6)
        }
    }
}

extension HomeDetailViewController: HomeDetailViewControllerProtocol {
    func rootViewConfigure(with data: HomeDetailRootView.Model) {
        rootView.configureView(model: data)
    }
}
