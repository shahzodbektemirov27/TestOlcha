//
//  HomeCoordinator.swift
//  TestOlcha
//
//  Created by Shahzod Bektemirov on 2024/04/09.
//

import UIKit

protocol HomeCoordinatorFlow: AnyObject {
    func didSelect(with id: Int)
}

final class HomeCoordinator: Coordinator {
    
    private let navigationController: UINavigationController
    
    init(navigationController: UINavigationController) {
        self.navigationController = navigationController
    }
    
    func start() {
        let diContainer = DIContainer.shared
        let presenter = HomePresenter(diContainer: diContainer)
        presenter.coordinator = self
        let homeViewController = HomeViewController(presenter: presenter)
        presenter.view = homeViewController
        navigationController.pushViewController(homeViewController, animated: true)
    }
}

extension HomeCoordinator: HomeCoordinatorFlow {
    func didSelect(with id: Int) {
        let homeDetailCoordinator = HomeDetailCoordinator(
            navigationController: navigationController,
            id: id
        )
        coordinate(to: homeDetailCoordinator)
    }
}
