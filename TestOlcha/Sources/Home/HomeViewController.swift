//
//  HomeViewController.swift
//  TestOlcha
//
//  Created by Shahzod Bektemirov on 2024/04/09.
//

import UIKit

extension HomeViewController {
    private enum Size {
        static let paddings: CGFloat = 10
        static let heightRowPadding: CGFloat = 80
    }
}

@MainActor
protocol HomeViewControllerProtocol: AnyObject {
    func reloadView()
    func endRefreshing()
}

final class HomeViewController: BaseViewController {
    // MARK: - UI Elements
    
    private lazy var refreshControl: UIRefreshControl = {
        let refresh = UIRefreshControl()
        refresh.tintColor = .red
        refresh.addTarget(self, action: #selector(handleUpdate), for: .valueChanged)
        return refresh
    }()
    
    private let searchBar = UISearchBar()
    private let tableView = UITableView()
    
    // Dependencies
    private let presenter: HomePresenter?
    
    // MARK: - Init
    
    init(presenter: HomePresenter) {
        self.presenter = presenter
        super.init()
    }
    
    // MARK: - Lifecycle
    
    override func viewDidLoad() {
        super.viewDidLoad()
        presenter?.viewDidload()
        configureUI()
    }
    
    @objc private func handleUpdate() {
        presenter?.didRefreshData()
    }
}

extension HomeViewController: HomeViewControllerProtocol {
    func reloadView() {
        self.tableView.reloadData()
    }
    
    func endRefreshing() {
        refreshControl.endRefreshing()
    }
}

// MARK: - UI Setup
extension HomeViewController {
    private func configureUI() {

        view.addSubview(searchBar)
        searchBar.delegate = self
        searchBar.placeholder = "Search"
        searchBar.searchBarStyle = .minimal
        searchBar.snp.makeConstraints {
            $0.top.equalTo(view.safeAreaLayoutGuide)
            $0.leading.trailing.equalToSuperview().inset(Size.paddings)
        }
        
        view.addSubview(tableView)
        tableView.dataSource = self
        tableView.delegate = self
        guard let presenterCellId = presenter?.cellIdentifier else { return }
        tableView.register(HomeTableViewCell.self, forCellReuseIdentifier: presenterCellId)
        tableView.rowHeight = Size.heightRowPadding
        tableView.separatorStyle = .none
        tableView.refreshControl = refreshControl
        
        tableView.snp.makeConstraints {
            $0.top.equalTo(searchBar.snp.bottom)
            $0.leading.trailing.bottom.equalToSuperview()
        }
    }
}

// MARK: - UITableView Data Source
extension HomeViewController: UITableViewDataSource, UITableViewDelegate {
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        guard let number = presenter?.numberOfRowsInSection() else { return 0 }
        return number
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        presenter?.didSelect(tableView: tableView, index: indexPath)
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let result = presenter?.cellForRowAt(index: indexPath.row, tableView: tableView) else {
            return UITableViewCell()
        }
        
        return result
    }
}

extension HomeViewController: UISearchBarDelegate {
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        presenter?.searchBar(searchBar: searchBar, searchText: searchText)
    }
    
    func searchBarTextDidBeginEditing(_ searchBar: UISearchBar) {
        UIView.animate(withDuration: 3, animations: { [weak self] in
            self?.searchBar.showsCancelButton = true
        })
    }
    
    func searchBarTextDidEndEditing(_ searchBar: UISearchBar) {
        UIView.animate(withDuration: 3, animations: { [weak self] in
            self?.searchBar.showsCancelButton = false
        })
    }
}
