//
//  HomePresenter.swift
//  TestOlcha
//
//  Created by Shahzod Bektemirov on 2024/04/09.
//

import Foundation
import UIKit
import CoreData

protocol HomePresenterProtocol: AnyObject {
    func viewDidload()
    func getHomePostList()
    func numberOfRowsInSection() -> Int
    func cellForRowAt(index: Int, tableView: UITableView) -> UITableViewCell
    func searchBar(searchBar: UISearchBar, searchText: String)
    func filterData(for searchText: String)
    func didSelect(tableView: UITableView, index: IndexPath)
    func didRefreshData()
    func saveAllData(name: String)
}

final class HomePresenter: HomePresenterProtocol {
    
    private let diContainer: DIContainer
   
    private var data: HomeResponseList? = HomeResponseList()
    private var filteredData: HomeResponseList? = HomeResponseList()
    private var isSearching: Bool = false
    private var cellAdded = false
    
    weak var view: HomeViewControllerProtocol?
    var coordinator: HomeCoordinatorFlow?
    
    var cellIdentifier: String {
        String(describing: HomeTableViewCell.self)
    }
    
    // MARK: - Init
    
    init(diContainer: DIContainer) {
        self.diContainer = diContainer
    }
    
    // MARK: - Lifecycle
    
    @MainActor func viewDidload() {
        getHomePostList()
    }
    
    @MainActor func didRefreshData() {
        getHomePostList()
    }
}

//CORE CONFIGURES
extension HomePresenter {
    func saveAllData(name: String) {
        let cellTitle = CRHomeResponse(context: diContainer.coreData.persistent.viewContext)
        cellTitle.title = name
        diContainer.coreData.saveContext()
    }
}

//TABLE CONFIGURES
extension HomePresenter {
    func numberOfRowsInSection() -> Int {
        return data?.count ?? 0
    }
    
    func cellForRowAt(index: Int, tableView: UITableView) -> UITableViewCell {
        guard let cell = tableView.dequeueReusableCell(
            withIdentifier: cellIdentifier,
            for: IndexPath(row: index, section: 0)
        ) as? HomeTableViewCell else { return UITableViewCell() }
        cell.selectionStyle = .none
        
        guard let data = isSearching ? filteredData : data, index < data.count else {
            return UITableViewCell()
        }
        
        let item = data[index]
        
        let titleComponents = item.title?.components(
            separatedBy: CharacterSet(charactersIn: " \n")
                .union(.newlines)
        ) ?? []
        let title = titleComponents.joined(separator: " ")
        
        cell.configure(.init(
            title: item.body ?? "",
            description: title
        ))
        
        return cell
    }
    
    func didSelect(tableView: UITableView, index: IndexPath) {
        guard let id = data?[index.row].id else { return }
        guard let title = data?[index.row].title else { return }

        let context = CoreData.shared.persistent.viewContext
        let newListItem = CRHomeResponse(context: context)
        newListItem.title = title
        var cellAdded = false
        if !cellAdded {
            diContainer.coreData.saveContext()
            cellAdded = true
        }

        coordinator?.didSelect(with: id)
    }
}

//SEARCH CONFIGURE
extension HomePresenter {
    @MainActor func searchBar(searchBar: UISearchBar, searchText: String) {
        isSearching = !searchText.isEmpty
        filterData(for: searchText)
    }
    
    @MainActor func filterData(for searchText: String) {
       
        guard let data = data else {
            filteredData = nil
            return
        }
        
        if searchText.isEmpty {
            filteredData = data
        } else {
            filteredData = data.filter {
                guard let title = $0.body?.lowercased() else {
                    return true
                }
                
                return title.contains(searchText.lowercased())
            }
        }
        
        view?.reloadView()
    }
}

//LOGIC
extension HomePresenter {
    @MainActor func getHomePostList() {
        view?.endRefreshing()
        Task.detached { [weak self] in
            guard let self else { return }
            
            do {
                let router = HomeRouter.postList
                let result: DataResponse<HomeResponseList> = try await diContainer.network.request(endPoint: router)
                
                let homeResponseList: HomeResponseList = result.model
                
                await MainActor.run {
                    self.data = homeResponseList
                    self.view?.reloadView()
                }
                
            } catch {
                print("Error: \(error)")
            }
        }
    }
}
