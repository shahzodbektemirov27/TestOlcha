//
//  HomeTableViewCell.swift
//  TestOlcha
//
//  Created by Shahzod Bektemirov on 2024/04/09.
//

import UIKit
import SnapKit

extension HomeTableViewCell {
    public struct Model {
        let title: String
        let description: String
    }
    
    private enum Size {
        static let padding: CGFloat = 10
        static let fontSize: CGFloat = 12
    }
}

final class HomeTableViewCell: TableViewCell {
    // MARK: - UI Elements
    
    private let rootView = UIStackView()
    let titleLabel = UILabel()
    private let descriptionLabel = UILabel()
    
    public func configure(_ model: Model) {
        titleLabel.text = model.title
        descriptionLabel.text = model.description
    }
    
    override func commonInit() {
        super.commonInit()
        configureUI()
    }
}

// MARK: - UI 
extension HomeTableViewCell {
    private func configureUI() {
        addSubview(rootView)
        rootView.spacing = Size.padding
        rootView.axis = .vertical
        rootView.alignment = .leading
        rootView.distribution = .fill
        rootView.layer.cornerRadius = Size.padding
        rootView.layer.borderColor = UIColor.lightGray.cgColor
        rootView.layer.borderWidth = 0.5
        rootView.configureMargin(margins: .init(top: 10, left: 10, bottom: 10, right: 10))
        
        rootView.snp.makeConstraints {
            $0.edges.equalToSuperview().inset(Size.padding)
        }
        
        rootView.addArrangedSubview(titleLabel)
        titleLabel.font = UIFont.systemFont(ofSize: Size.fontSize, weight: .regular)
        titleLabel.textColor = UIColor.black
        titleLabel.font = UIFont.systemFont(ofSize: 14, weight: .medium)
        titleLabel.numberOfLines = 1
        titleLabel.textAlignment = .left
        
        rootView.addArrangedSubview(descriptionLabel)
        descriptionLabel.font = UIFont.systemFont(ofSize: Size.fontSize, weight: .regular)
        titleLabel.textColor = UIColor.black
        titleLabel.font = UIFont.systemFont(ofSize: 12, weight: .medium)
        descriptionLabel.numberOfLines = 1
        descriptionLabel.textAlignment = .left
    }
}
