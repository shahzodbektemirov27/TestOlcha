//
//  HomeDetailRootView.swift
//  TestOlcha
//
//  Created by Shahzod Bektemirov on 2024/04/10.
//

import UIKit

extension HomeDetailRootView {
    private enum Size {
        static let labelHeightPadding: CGFloat = 30
        static let titleLabelHeightPadding: CGFloat = 50
    }
    
    public struct Model {
        let userId: Int
        let id: Int
        let title: String
        let description: String
    }
}

final public class HomeDetailRootView: UIStackView {
    
    // UI
    private let userIdLabel = UILabel()
    private let idLabel = UILabel()
    private let titleLabel = UILabel()
    private let bodyLabel = UILabel()
      
    override init(frame: CGRect) {
        super.init(frame: frame)
        setUpUI()
    }

    @available(*, unavailable)
    required init(coder _: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    // MARK: - Public funcs
    func configureView(model: Model) {
        userIdLabel.text = String(model.userId)
        idLabel.text = String(model.id)
        titleLabel.text = model.title
        bodyLabel.text = model.description
    }
}

extension HomeDetailRootView {
    private func setUpUI() {
        axis = .vertical
        spacing = 5
        distribution = .fill

        userIdLabel.snp.makeConstraints {
            $0.height.equalTo(Size.labelHeightPadding)
        }
        
        idLabel.snp.makeConstraints {
            $0.height.equalTo(Size.labelHeightPadding)
        }
        
        titleLabel.snp.makeConstraints {
            $0.height.equalTo(Size.titleLabelHeightPadding)
        }
        
        [userIdLabel, idLabel, titleLabel, bodyLabel].forEach {
            addArrangedSubview($0)
            $0.font = UIFont.systemFont(ofSize: 16, weight: .medium)
            $0.textColor = .black
            $0.numberOfLines = 0
            $0.textAlignment = .center
        }
    }
}
