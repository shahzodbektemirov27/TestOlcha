//
//  TableViewCell.swift
//  TestOlcha
//
//  Created by Shahzod Bektemirov on 2024/04/09.
//

import UIKit

open class TableViewCell: UITableViewCell {
    
    // MARK: - Lifecycle
    
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        commonInit()
    }
    
    required public init?(coder: NSCoder) {
        fatalError("Error has occured")
    }
    
    open func commonInit() {
        
    }
}
