//
//  Extension + UIStackView.swift
//  TestOlcha
//
//  Created by Shahzod Bektemirov on 2024/04/09.
//

import UIKit

extension UIStackView {
    
    public var margins: UIEdgeInsets {
        get { layoutMargins }
        set { layoutMargins = newValue }
    }
    
    public func configureMargin(margins: UIEdgeInsets = .zero) {
        self.margins = margins
        self.isLayoutMarginsRelativeArrangement = true
    }
}
