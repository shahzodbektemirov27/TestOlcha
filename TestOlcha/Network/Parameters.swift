//
//  Parameters.swift
//  TestOlcha
//
//  Created by Shahzod Bektemirov on 2024/04/09.
//

import Foundation

public typealias Parameters = [String: Any]

protocol ParameterEncoding {
   
    func encode(
        _ urlRequest: URLRequestConvertible,
        with parameters: Parameters?
    ) throws -> URLRequest
}
