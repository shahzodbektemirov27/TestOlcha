//
//  JSON.swift
//  TestOlcha
//
//  Created by Shahzod Bektemirov on 2024/04/09.
//

import Foundation

struct JSON {
    
    private let object: Any
    
    init(_ object: Any) {
        self.object = object
    }
    
    var prettyPrinted: String? {
        do {
            let obj: Any
            if let data = object as? Data {
                obj = try JSONSerialization.jsonObject(with: data, options: [])
            } else {
                obj = object
            }
            let data = try JSONSerialization.data(
                withJSONObject: obj,
                options: .prettyPrinted
            )
            return String(data: data, encoding: .utf8)
        } catch {
            return nil
        }
    }
}

extension JSON: Swift.CustomStringConvertible, Swift.CustomDebugStringConvertible {
    var description: String {
        prettyPrinted ?? "unknown"
    }
    
    var debugDescription: String {
        description
    }
}
