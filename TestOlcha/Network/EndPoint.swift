//
//  EndPoint.swift
//  TestOlcha
//
//  Created by Shahzod Bektemirov on 2024/04/09.
//

import UIKit

protocol Router: URLRequestConvertible, CustomStringConvertible {
    var path: String { get }
    var parameters: Parameters { get }
    var method: HTTPMethod { get }
    var timeout: Double { get }
    var cacheable: Bool { get }
}

extension Router {
    public var urlString: URL {
        var url: URL?
        url = URL(string: NetworkConstants.dev.rawValue)
        url = url?.appendingPathComponent(self.path)
        guard let url else {
            assertionFailure()
            return .init(fileReferenceLiteralResourceName: "")
        }
        return url
    }
    
    public func asURLRequest() throws -> URLRequest {
        var urlRequest = URLRequest(url: self.urlString)
        urlRequest.httpMethod = self.method.rawValue
        urlRequest.allHTTPHeaderFields = self.headers
        urlRequest.timeoutInterval = self.timeout
        return urlRequest
    }
    
    public var headers: [String: String] {
//        let diContainer = DIContainer.init(
//            network: NetworkService.shared,
//            appData: AppManager.shared
//        )
        
        var data = [String: String]()
        return data
    }
    
    var description: String {
        """
        url: \(self.urlString)
        method: \(self.method.rawValue)
        headers: \(JSON(self.headers))
        parameters: \(JSON(self.parameters))
        """
    }
    
    var timeout: Double {
        return 60
    }
    
    var cacheable: Bool {
        true
    }
}

