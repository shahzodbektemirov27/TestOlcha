//
//  HomeRouters.swift
//  TestOlcha
//
//  Created by Shahzod Bektemirov on 2024/04/09.
//

import Foundation

enum HomeRouter: Router {
    case postList
    case postDetailList(id: Int)
    
    var path: String {
        switch self {
        case .postList:
            return "/posts"
        case .postDetailList(let id):
            return "/posts/\(id)"
        }
    }
    
    var method: HTTPMethod {
        switch self {
        case .postList, .postDetailList:
            return .get
        }
    }
    
    var parameters: Parameters {
        switch self {
        case .postList, .postDetailList:
            return [:]
        }
    }
}
