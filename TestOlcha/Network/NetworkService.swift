//
//  NetworkService.swift
//  TestOlcha
//
//  Created by Shahzod Bektemirov on 2024/04/09.
//

import UIKit

public struct DataResponse<T: Decodable> {
    public let model: T
}

protocol AnyNetworkService {
    @discardableResult
    func request<T: Decodable>(endPoint: Router) async throws -> DataResponse<T>
}

public final class NetworkService: AnyNetworkService {
    
    static let shared = NetworkService()
    
    @discardableResult
    func request<T>(endPoint: Router) async throws -> DataResponse<T>  {
        
        let urlRequest = try endPoint.asURLRequest()
        let (data, _) = try await URLSession.shared.data(for: urlRequest)
        let decoder = JSONDecoder()
        let model = try decoder.decode(T.self, from: data)
        return DataResponse(model: model)
    }
}

