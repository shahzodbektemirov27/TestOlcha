//
//  NetworkConstants.swift
//  TestOlcha
//
//  Created by Shahzod Bektemirov on 2024/04/09.
//

import Foundation

public enum NetworkConstants: String {
    
    case dev = "https://jsonplaceholder.typicode.com"
}

