//
//  HTTPMethods.swift
//  TestOlcha
//
//  Created by Shahzod Bektemirov on 2024/04/09.
//

import Foundation

enum HTTPMethod: String, CaseIterable {
    case get = "GET"
    case post = "POST"
    case put = "PUT"
    case delete = "DELETE"
}

